const jimp = require('jimp');
const _ = require('lodash');
const colors = require('./data/colors.json');
const csv = require('fast-csv');
const fs = require('fs');

let C = {
  cell_width: 28.65,
  cell_height: 28.65,
  top_x: 128,
  top_y: 265,
  row_count: 650,
  column_count: 440
};

let notableColors = {};
let oceanColors = {};
let wsHeight = fs.createWriteStream("./generated/result_height.csv");
let wsOcean = fs.createWriteStream("./generated/result_ocean.csv");

(function init(){
  let hexColor, i, rgb;
  jimp.read('./data/ynev.png', process);
  for(i=0; i<colors.length; i+=1){
    rgb = colors[i];
    hexColor = jimp.rgbaToInt(rgb.r, rgb.g, rgb.b, 255);
    notableColors[`_${hexColor}`] = rgb.height;
    if(i==0){
      oceanColors[`_${hexColor}`] = rgb.height;
    }
  }
}());

function process(err, image){
  if(err){
    console.log(err);
    return;
  }
  processData(image, {colors: notableColors, proc: getMean, file: wsHeight});
  processData(image, {colors: oceanColors, proc: getOceanPercentage, file: wsOcean});
}

function processData(image, options){
  let imageWidth = image.bitmap.width;
  let imageHeight = image.bitmap.height;
  let cellWidth = Math.round(C.cell_width);
  let cellHeight = Math.round(C.cell_height);
  let colorCollection = [];
  let rowIndex, columnIndex, cellX, cellY, colorMean, currentRow;
  for(rowIndex = 0; rowIndex < C.row_count; rowIndex+=1){
    colorCollection.push([]);
    currentRow = _.last(colorCollection);
    for(columnIndex = 0; columnIndex < C.column_count; columnIndex+=1){
      cellX = C.top_x + Math.round(C.cell_width * columnIndex);
      cellY = C.top_y + Math.round(C.cell_height * rowIndex);
      if(cellX + cellWidth < imageWidth && cellY + cellHeight < imageHeight){
        colorMean = getColor(options, image, cellX, cellY, cellWidth, cellHeight);
        currentRow.push(colorMean);
      }
    }
  }
  csv
    .write(colorCollection, {delimiter: ';'})
    .pipe(options.file);
}

function getColor(options, image, cellX, cellY, cellWidth, cellHeight){
  let {colors, proc} = options;
  let x, y, hexColor;
  let values = [];
  for(x = cellX; x<=cellX+cellWidth; x+=1){
    for(y = cellY; y<=cellY+cellHeight; y+=1){
      hexColor = '_'+image.getPixelColor(x, y);
      if(typeof colors[hexColor] != 'undefined'){
        values.push(colors[hexColor]);
      }
    }
  }
  return proc(values);
}

function getOceanPercentage(values){
  return Math.round(values.length*1000/900);
}

function getMean(heights){
  let mean = _.mean(heights);
  if(isNaN(mean)){
    return -1;
  }
  return _.round(mean);
}
